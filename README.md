In light of recent escalation in identity theft and slander, all documents and petitions in which I've granted third parties to use my signature or name will be included here. These cases pertain particularly to Mexico's National Autonomous Univerity (UNAM) -School of Economics-, in which, despite repeated offenses, authorities have taken no action to ensure a safe work environment. Thus, if I endorse a document with my signature, it will be included in this git, for I have the courage to openly speak to the causes I support. 

If a file is included a SHA512 hash file and its detached gpg signature will be included, whereas in absence thereof, the body of the text will be included in the form of a clearsigned document. As white spaces are also processed in the signing procedure, I caution individuals verifying my signatures to be mindful of thoroughly reviewing its contents as discrepancies may exist between a text online and the text I agreed to sign.

Por el aumento de casos de usurpación de identidad y difamación, todos los documentos y pronunciamientos en los que he dado mi autorización a otras personas serán incluidos aquí. Estos casos son particularmente en la Universidad Nacional Autónoma de México (en la Facultad de Economía), que, a pesar de que se han usurpado identidades en más de una ocasión, las autoridades no han garantizado un espacio seguro de trabajo. Por lo tanto, si suscribo un documento con mi firma, estará incluída en este git, porque yo sí tengo el valor de hablar abiertamente de las causas con las que simpatizo.

Si incluyo un archivo, también se incluirá un hash SHA512 con su respectiva firma gpg detached, mientras que en ausencia de un archivo, se incluirá el cuerpo del texto en la forma de un “clearsign”. Como los espacios y otros caractéres como las nuevas líneas también son firmadas con gpg, le solicito a las personas verificando estos documentos de revisar el contenido del documento firmado, dado que pueden existir discrepancias entre el documento que accedí a firmar y el que está en línea.

Possible names - Posibles nombres:

Samuel J. Rosado

Samuel Rosado Zaidi
Samuel Rosado-Zaidi

