-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

A la comunidad de la Facultad de Economía.
Al igual que cada cuatro años, en la Facultad de Economía se renovará la dirección de la institución. Se trata de un proceso que, en gran medida, escapa a la voluntad de sus miembros puesto que recae la decisión en instancias sobre las cuales la comunidad prácticamente no tiene ninguna incidencia.
Sin embargo, profesoras y profesores de distintas áreas académicas, consideramos indispensable definir, por una parte, la situación actual de la Facultad así como, por otra, los cambios que deben emprenderse con el objeto de revertir la grave situación en que nos encontramos actualmente.
    I. El deterioro del ambiente académico nos parece indiscutible. Ciertamente, la pandemia y su correlato de cierre de las instalaciones tuvo un fuerte impacto en la disgregación de la comunidad, aunque este último fenómeno se venía registrando desde tiempo atrás. Nos referiremos a él más adelante. Una vez superada la coyuntura sanitaria, nos enfrentamos a un ciclo de huelgas y paros ante los cuales la conducción de la Facultad por sus órganos directivos fue de franca atonía. Resalta la permisividad y tolerancia de la dirección ante acciones arbitrarias de grupos de interés, estudiantiles y de docentes. El remedio paliativo de proseguir clases en línea no hizo sino profundizar la crítica situación manifiesta, entre otros, en la pérdida de hábitos de estudio de los alumnos y en el ausentismo del cuerpo docente.
Fue así como quedó suspendida la reforma del plan de estudios, misma que registraba sustanciales avances desde hace más de un lustro en varias áreas de la División de Estudios Profesionales y que implicó un fructífero, aunque efímero diálogo, entre profesores. Dicha reforma fue prácticamente abandonada por la administración saliente.
La fragmentación de la vida académica debe atribuirse, indudablemente, a las modalidades de remuneración salarial vigentes que individualizan el esfuerzo de los y las docentes en pos de alcanzar niveles más altos de ingreso y a las precarias condiciones del personal de asignatura. No obstante, hay otras causas entre las que destaca la conversión de lo que fueron antaño las academias en áreas cuyos titulares son en los hechos relevos de la dirección y personal de confianza de ésta. Devinieron así instancias burocrático administrativas y abandonaron su función primordial de ser espacios de diálogo académico entre estudiantes y profesores con inquietudes intelectuales y preocupaciones magisteriales compartidas.
    II. El diagnóstico anterior nos debe conminar a un serio replanteamiento de las bases mismas de la dinámica académica de la Facultad.
La exigencia actual de los estudiantes de impulsar una reforma del plan curricular ya que éste cuenta con tres décadas de vigencia y la urgencia de su actualización debe ser retomada, pero nos oponemos a que se inicie sin una amplia y profunda reflexión acerca de la metodología que la guiará.
Esta metodología debe descansar no sólo en las modalidades operativas de la reforma, sino y fundamentalmente en la definición de los problemas nacionales sustentada en el pensamiento crítico que ha caracterizado a nuestra Facultad desde su fundación, pero que se ha desdibujado en los últimos tiempos.
Asimismo, consideramos legítima la demanda estudiantil de restablecer la paridad en el Consejo Técnico. Se trata, desde nuestra perspectiva, de una tradición democrática que a lo largo de varias décadas permitió establecer consensos entre diversos grupos de la Facultad y entre estudiantes y profesores. Amén de lo anterior, ello permitirá una mayor transparencia en el uso de los recursos económicos, y la asignación de las plazas de profesores y el banco de horas, cuya opacidad actual constituye para nosotras y nosotros una severa preocupación. 
Por último, insistimos en la necesidad de mejorar la situación laboral de las y los profesoras y profesores de asignatura que representan un porcentaje mayoritario de la planta docente. 
Invitamos a las y los candidatas y candidatos a asumir la dirección de la FE, a entablar un diálogo con la comunidad en torno a los puntos enunciados y explicitar cómo cristalizarán en su propuesta.

“POR MI RAZA HABLARÁ EL ESPÍRITU”
-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEEoDZWhsyV+bM9opmmhVwUJnMs8UUFAmWmECIACgkQhVwUJnMs
8UWCDg/9Gyhfew2pEweMoXmWiqqzUbo1nnoDa+3fXjvGEaZW9lvcg8VS+/PwsjhB
mvKhUznAa3gkcIRPEEe0Y/Am4PT66/zOElWR3fI5Cd6Ls1vSG5nvDYQHXYOJwS+b
H5n1gsdC7xyFnNzXFsP2D86cBhsBxxrZwwHEqq1GcqPHib8Hdx965GJsu/WsFTAB
hdHpTEW8gy6CqjZ5MhcPC3b4xCD0gdTy7nI+fLmIhMTeLk+zmpbag327wrBujUhx
r4fITFsC/3+HzuPfJPtBgf3SrANZv1M0lHDYmCO5ui35igT23zvRrfhOe17kCHJ4
u4Oe7DDkBj8/XajEF8lHIP6EvLRERVITAAKwq8L1HQGlVWHpjuoo6njGCKeMZqkX
fVhZetIXPzBkhnV9xPEMb3Ui7mI2fP03s2NV93fD3OW49r/bP2u7yvw03PfSUJu+
K8rbfHyR7FrtQXOF9cdMhiZcv1JCwEqcv8a6EpS/crmKlhKIvapyR5rdnjQxz2px
Pwy3RGmz+S030rypeZCF0Snlxx1MxLx5/vOXzrmJrAN+zvVr/bcxC+tYixvcXJWk
1AQsFF1MiV8JkPTidcrQHCzA8+LPD/ndytLxZMRud63+yfCQPB+eUhSmoi5F809x
aWdjotHJ7NWT1KewmqGp2qnYSC0+KncHwRkPowGhIDCI/HOXikU=
=uWys
-----END PGP SIGNATURE-----
