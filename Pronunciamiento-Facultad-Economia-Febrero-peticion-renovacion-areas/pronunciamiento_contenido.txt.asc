-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Mtra. Lorena Rodríguez					At´n Consejo Técnico
Directora de la Facultad de Economía, UNAM
Presente.
En el reciente proceso de renovación de la dirección de la Facultad, profesoras y profesores hicimos llegar a los integrantes de la Terna enviada por el Rector, una misiva, en la cual presentamos un diagnóstico y propuestas, que solicitamos fueran incorporadas por quien ocupara la dirección.
Una de ellas, que de manera unánime, también fue apoyada por las y los asistentes al Auditorio Ho Chi Minh,  el 24 de enero del año en curso, durante la presentación de los  programas de trabajo de los integrantes de la terna, fue la necesidad inmediata de recuperar la vida colegiada de las academias, recuperando la estructura de academias en todas las áreas. Reintegrando a las y los académicos de sus respectivas áreas, la potestad de elegir a su  coordinador o coordinadora, al tiempo de generar líneas de trabajo, en las que sea obligatorio un funcionamiento democrático y colegiado en todas las decisiones que atañen a la vida académica: programas y planes de estudio, seminarios, proyectos de investigación colectivos, apertura de las publicaciones a todas las perspectivas teóricas de las investigaciones de los profesores, y los espacios de difusión y extensión,  atención a los estudiantes, revisión de las condiciones de estudio, mejora de las condiciones de trabajo del personal académico, en especial el de asignatura, transparencia en las plazas vacantes, garantía de concursos de oposición imparciales y la eliminación de candados para la representación al Consejo Técnico, entre otros aspectos.
Es por ello que solicitamos a usted y al Consejo Técnico la apertura inmediata de un proceso de elección de las coordinaciones de las áreas académicas, en donde las profesoras y profesores, por voto universal, directo y secreto decidamos quienes ocuparán éstas.
 Esperamos su pronta respuesta
“POR MI RAZA HABLARÁ EL ESPÍRITU”
Cd. Universitaria, CDMX,  a 20 de febrero de 2024
Profesoras y profesores de la Facultad de Economía.
-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEEoDZWhsyV+bM9opmmhVwUJnMs8UUFAmXX+vQACgkQhVwUJnMs
8UUC7g/6AsFreDE6uxHzbXndglBpnKmI6lTDVjMyfrLAFobx1KrFfDGb0tDu2x2s
94Jq04O8dl140BbPF237qTVWVrW7ypIJMBBIWod1qLEd+yhc7/Y7hJcX5OQ4JNFE
x3V2yFXiMhAP6GXWjp9Fb4sxiJ+7Qx4rR9OHQj2eEwHVNyKfYelGWkydCF36YYpR
oiyWhv4yPNRvzARHQYHBerWFyYITbIewOKWspnP3s2uFmR8hwxjSnh2YefqIfF9k
kJo15593cMZLfdkJY71UuVbkGeajgNnayHpGZ67tqthNnrWMc2bNL2CvN/88p2gD
8BfnXN4wD4gKoLnAoqPGYRO6zbhC5rxSskLyiuQBIb31UZZ3599ePEcoa7Mit/ds
usa8IqEID0+RO0lYv+rLMqjR166kED94c9ehteRQ/oQi63+Rd9oIFOoxSnlub2YW
LJVNo1WpPxD+rB9SOb5P2AtmLKKcjbgjgfQwLJWZduAqfRIeY8F6IW5xTizvfDg+
d7ZMBuVqwEiLDcDb729nxwtIrtJcs5oNEfE4Gq4RQpg0tAW505VUzACnQS+6ByvU
IlzUE5zB59LpypGpep0lRrgmyDmnqwI0+Y1USetNakEEHl82aSAfgp4DfwuUvOlL
O4vCB29weVRi88PmDETRzZKl6UcBehuD4kbf5wlsGIR9MbS7owA=
=ZxkH
-----END PGP SIGNATURE-----
